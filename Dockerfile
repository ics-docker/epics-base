FROM registry.esss.lu.se/ics-docker/miniconda

RUN conda install -y -n base \
    tini \
  && conda clean -ay

RUN conda create -y -n epics -c conda-e3-virtual \
    python=3.7 \
    epics-base=7.0.3 \
    pvapy=2.0 \
    pyepics=3.4 \
  && conda clean -ay

COPY entrypoint /opt/docker/bin/entrypoint

ENV EPICS_CA_AUTO_ADDR_LIST=NO \
    EPICS_PVA_AUTO_ADDR_LIST=NO

# The entrypoint will activate the conda environment
ENTRYPOINT [ "/opt/conda/bin/tini", "--", "/opt/docker/bin/entrypoint" ]
CMD [ "/bin/bash" ]

# epics-base docker image

Docker image including epics-base 7 and pyepics/pvapy.
The `epics` conda environment is activated by default.


Docker pull command:

```
docker pull registry.esss.lu.se/ics-docker/epics-base:latest
```

## Usage

Start a container to run some EPICS commands:
```
$ docker run --rm -it registry.esss.lu.se/ics-docker/epics-base
[conda@273b3490637a /]$ export EPICS_CA_ADDR_LIST=<ip>
[conda@273b3490637a /]$ caget MYPV
[conda@273b3490637a /]$ export EPICS_PVA_ADDR_LIST=<ip>
[conda@273b3490637a /]$ pvget MYPV
```

You can even run a one line command. But don't forget to export your `EPICS_CA_ADDR_LIST` or `EPICS_PVA_ADDR_LIST`.

```
$ docker run --rm -it -e EPICS_CA_ADDR_LIST=<ip> registry.esss.lu.se/ics-docker/epics-base caget MYPV
```
